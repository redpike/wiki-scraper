import os

from flask_httpauth import HTTPBasicAuth

from util.singleton import Singleton


class SecurityConfiguration(metaclass=Singleton):
    users: {str: str} = {}
    roles: {str: str} = {}
    auth = HTTPBasicAuth()

    def __init__(self) -> None:
        admin_user: str = os.environ.get('ADMIN_USER')
        admin_pass: str = os.environ.get('ADMIN_PASS')
        requester_user: str = os.environ.get('REQUESTER_USER')
        requester_pass: str = os.environ.get('REQUESTER_PASS')

        SecurityConfiguration.users.update({
            admin_user: admin_pass,
            requester_user: requester_pass
        })

        SecurityConfiguration.roles.update({
            admin_user: 'role_admin',
            requester_user: 'role_requester'
        })

    @staticmethod
    @auth.verify_password
    def verify_password(username: str, password: str) -> bool:
        user_password: str = SecurityConfiguration.users.get(username)
        return user_password == password

    @staticmethod
    @auth.get_user_roles
    def permission_required(user) -> str:
        return SecurityConfiguration.roles.get(user.username)
