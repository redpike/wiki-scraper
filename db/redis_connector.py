import os

from redis import StrictRedis

from util.singleton import Singleton


class RedisConnector(metaclass=Singleton):

    def __init__(self) -> None:
        host = os.getenv('REDIS_HOST')
        port = int(os.getenv('REDIS_PORT'))
        password = os.getenv('REDIS_PASSWORD')
        if password is None:
            self.session: StrictRedis = StrictRedis(host=host, port=port, db=0)
        else:
            self.session: StrictRedis = StrictRedis(host=host, port=port, password=password, db=0)

    def set(self, key: str, value: str) -> None:
        self.session.set(key, value)
        self.session.close()

    def get(self, key: str) -> str:
        value = self.session.get(key)
        self.session.close()
        return value
