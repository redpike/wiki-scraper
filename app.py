import logging

from flask import Flask

from resources.upgrade.upgrades import upgrades

app: Flask = Flask(__name__)
app.register_blueprint(upgrades, url_prefix='/api/sfh/v1')


if __name__ == '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

    app.run()
