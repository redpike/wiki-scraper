import json
import time
from typing import Final


class CardType:
    COMMON = 'COMMON'
    RARE = 'RARE'
    EPIC = 'EPIC'


class UpgradeDataEntry:

    def __init__(self, level: str, cards: str, gold: str) -> None:
        self._level = level
        self._cards = cards
        self._gold = gold

    @property
    def level(self) -> str:
        return self._level

    @level.setter
    def level(self, value) -> None:
        self._level = value

    @property
    def cards(self) -> str:
        return self._cards

    @cards.setter
    def cards(self, value) -> None:
        self._cards = value

    @property
    def gold(self) -> str:
        return self._gold

    @gold.setter
    def gold(self, value) -> None:
        self._gold = value

    def __str__(self) -> str:
        return f'Level {self._level}, Cards {self._cards}, Gold {self._gold}'


class UpgradeData:

    def __init__(self) -> None:
        self.entries: {CardType, {str, UpgradeDataEntry}} = {}
        self.timestamp = int(time.time())

    def add_new_entry(self, type_of_card: CardType, level: str, cards: str, gold: str) -> None:
        parsed_level: Final[str] = level
        parsed_cards: Final[str] = '0' if cards == 'Unlock' or cards == 'Full' else cards
        parsed_gold: Final[str] = '0' if cards == 'Unlock' or cards == 'Full' else gold
        entry: UpgradeDataEntry = UpgradeDataEntry(parsed_level, parsed_cards, parsed_gold)

        if type_of_card in self.entries:
            self.entries[type_of_card][int(level)] = entry
        else:
            level_upgrade = {int(level): entry}
            self.entries[type_of_card] = level_upgrade

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)
