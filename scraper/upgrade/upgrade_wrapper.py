from bs4 import ResultSet, Tag

from db.redis_connector import RedisConnector
from scraper.html_wrapper.html_wrapper import HtmlWrapper
from scraper.upgrade.upgrade_data import UpgradeData, CardType

SITE_URL = 'https://smashingfour.fandom.com/wiki/Category:Currencies'


class UpgradeWrapper:

    @staticmethod
    def get_last_upgrade_costs_from_db() -> str:
        redis_connector: RedisConnector = RedisConnector()
        return redis_connector.get('upgrades')

    @staticmethod
    def wrap_upgrade_costs() -> None:
        page = HtmlWrapper.unwrap_html(SITE_URL)
        content = page.soup
        article_tables: ResultSet[Tag] = HtmlWrapper.unwrap_elements(content, '.article-table')
        upgrade_table_html: Tag = article_tables[2]
        upgrade_rows: ResultSet[Tag] = upgrade_table_html.find_all('tr')[1::]
        data: UpgradeData = UpgradeWrapper.parse_data(upgrade_rows)
        redis_connector: RedisConnector = RedisConnector()
        redis_connector.set('upgrades', data.to_json())

    @staticmethod
    def parse_data(upgrade_rows: ResultSet[Tag]) -> UpgradeData:
        upgrade_data: UpgradeData = UpgradeData()
        for row in upgrade_rows:
            cells: ResultSet[Tag] = row.find_all('td')
            UpgradeWrapper.parse_common_data(upgrade_data, cells)
            UpgradeWrapper.parse_rare_data(upgrade_data, cells)
            UpgradeWrapper.parse_epic_data(upgrade_data, cells)
        return upgrade_data

    @staticmethod
    def parse_common_data(upgrade_data: UpgradeData, cells: ResultSet[Tag]) -> None:
        level: str = UpgradeWrapper.remove_unnecessary_chars(HtmlWrapper.unwrap_specific_element_content(cells[0]))
        cards: str = UpgradeWrapper.remove_unnecessary_chars(HtmlWrapper.unwrap_specific_element_content(cells[1]))
        gold: str = UpgradeWrapper.remove_unnecessary_chars(HtmlWrapper.unwrap_specific_element_content(cells[4]))
        upgrade_data.add_new_entry(CardType.COMMON, level, cards, gold)

    @staticmethod
    def parse_rare_data(upgrade_data: UpgradeData, cells: ResultSet[Tag]) -> None:
        level: str = UpgradeWrapper.remove_unnecessary_chars(HtmlWrapper.unwrap_specific_element_content(cells[0]))
        cards: str = UpgradeWrapper.remove_unnecessary_chars(HtmlWrapper.unwrap_specific_element_content(cells[2]))
        gold: str = UpgradeWrapper.remove_unnecessary_chars(HtmlWrapper.unwrap_specific_element_content(cells[4]))
        upgrade_data.add_new_entry(CardType.RARE, level, cards, gold)

    @staticmethod
    def parse_epic_data(upgrade_data: UpgradeData, cells: ResultSet[Tag]) -> None:
        level: str = UpgradeWrapper.remove_unnecessary_chars(HtmlWrapper.unwrap_specific_element_content(cells[0]))
        cards: str = UpgradeWrapper.remove_unnecessary_chars(HtmlWrapper.unwrap_specific_element_content(cells[3]))
        gold: str = UpgradeWrapper.remove_unnecessary_chars(HtmlWrapper.unwrap_specific_element_content(cells[4]))
        upgrade_data.add_new_entry(CardType.EPIC, level, cards, gold)

    @staticmethod
    def remove_unnecessary_chars(string: str) -> str:
        return string.replace('\n', '').replace(' ', '')
