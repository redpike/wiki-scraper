from bs4 import BeautifulSoup, ResultSet, Tag
from mechanicalsoup import Browser
from requests import ReadTimeout, Session


class HtmlWrapper:
    session: Session = Session()
    browser: Browser = Browser(session=session)
    request_counter = 0

    @staticmethod
    def unwrap_html(site_url: str):
        try:
            page = HtmlWrapper.browser.get(site_url, timeout=20)
            HtmlWrapper.request_counter = 0
            HtmlWrapper.session.close()
            HtmlWrapper.browser.close()
            return page
        except ReadTimeout:
            if HtmlWrapper.request_counter == 3:
                raise ReadTimeout
            HtmlWrapper.request_counter += 1
            print()
            print('Fetch ' + site_url + ' -> retry no.' + str(HtmlWrapper.request_counter))
            return HtmlWrapper.unwrap_html(site_url)

    @staticmethod
    def unwrap_elements(html: BeautifulSoup, selector: str) -> ResultSet[Tag]:
        result_set: ResultSet[Tag] = html.select(selector)
        return result_set

    @staticmethod
    def unwrap_specific_element_content(tag: Tag) -> str:
        return str(tag.text)

    @staticmethod
    def unwrap_specific_element_content_to_string(html: BeautifulSoup, selector: str, index: int = 0) -> str:
        optional = HtmlWrapper.unwrap_elements(html, selector)
        content = ''
        if len(optional) > index:
            content = str(optional[index].text)
        return content

    @staticmethod
    def unwrap_attribute_value(tag: Tag, attribute_name: str) -> str:
        return str(tag.get(attribute_name))

    @staticmethod
    def unwrap_attribute_value_if_exist(elements: ResultSet[Tag], attribute_name: str,
                                        find_next: str = None) -> str:
        elements_counter = len(elements)
        result = ''
        if elements_counter > 0:
            if find_next:
                result = HtmlWrapper.unwrap_attribute_value(elements[0].find_next(find_next), attribute_name)
            else:
                result = HtmlWrapper.unwrap_attribute_value(elements[0], attribute_name)
        return result
