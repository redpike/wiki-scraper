from flask import Response, Blueprint, current_app
from werkzeug.local import LocalProxy

from flask_util.security_configuration import SecurityConfiguration
from scraper.upgrade.upgrade_wrapper import UpgradeWrapper

upgrades = Blueprint('upgrades', __name__)
api_security = SecurityConfiguration()
auth = api_security.auth

logger = LocalProxy(lambda: current_app.logger)


@upgrades.route('/upgrades', methods=['GET'])
@auth.login_required(role=['role_admin'])
def get_last_upgrades():
    try:
        data = UpgradeWrapper.get_last_upgrade_costs_from_db()
        if data is None:
            UpgradeWrapper.wrap_upgrade_costs()
            data = UpgradeWrapper.get_last_upgrade_costs_from_db()
        response = Response(data, 200, mimetype='application/json')
        logger.debug('Data fetched successfully [' + str(data) + ']')
    except Exception as e:
        logger.warn('Something went wrong: ' + str(e))
        response = Response('Something went wrong', 404)

    return response
