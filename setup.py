import os

from setuptools import setup

__version__ = '1.0.0'


def read(filename):
    return open(os.path.join(os.path.dirname(__file__), filename)).read()


setup(
    name="Smashing Four Helper Wiki Scraper",
    version=__version__,
    author="Code Beat",
    author_email="code.beat@outlook.com",
    description="Scraping Application for fetching all data Smashing Four Wiki",
    license="Freeware",
    packages=['src'],
    long_description=read('README'),
    classifiers=[
        "Development Status :: Released :: " + __version__,
        "Topic :: Utilities",
        "License :: Freeware",
    ]
)
