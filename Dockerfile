FROM python:3.10.7-alpine

WORKDIR /app

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

ENV GUNICORN_CMD_ARGS="--bind=0.0.0.0:9100 --log-level=INFO --chdir=./"
COPY . .

CMD [ "gunicorn", "app:app" ]