from unittest import TestCase

from bs4 import ResultSet, Tag

from scraper.html_wrapper.html_wrapper import HtmlWrapper
from scraper.upgrade.upgrade_data import UpgradeData
from scraper.upgrade.upgrade_wrapper import UpgradeWrapper, SITE_URL


class UpgradeWrapperTest(TestCase):

    def test_parse_data_should_parse_from_wiki(self):
        page = HtmlWrapper.unwrap_html(SITE_URL)
        content = page.soup
        article_tables: ResultSet[Tag] = HtmlWrapper.unwrap_elements(content, '.article-table')
        upgrade_table_html: Tag = article_tables[2]
        upgrade_rows: ResultSet[Tag] = upgrade_table_html.find_all('tr')[1::]
        data: UpgradeData = UpgradeWrapper.parse_data(upgrade_rows)
        self.assertEqual(len(data.entries.keys()), 3)
